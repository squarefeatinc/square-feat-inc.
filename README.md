Square Feat Inc. Commercial Cleaning Service is your first choice for professional commercial cleaning services for the Phoenix area. We offer janitorial services including regularly scheduled building maintenance, floor refinishing, commercial carpet cleaning and more. Call (480) 777-0605 for more.

Address: 2325 W Guadalupe Rd, #101, Gilbert, AZ 85233

Phone: 480-777-0605
